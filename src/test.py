import requests
import base64
import os

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
docker_url = 'http://172.17.0.2:80/predict'
local_url = 'http://127.0.0.1:5000/predict'


def process():
    img_path = os.path.join(BASE_PATH, 'test_images/2.jpg')
    encodedImage = base64.b64encode(open(img_path, "rb").read()).decode()
    payload = {
        'img_string': encodedImage,
        'detector': True
    }

    response = requests.post(local_url, json=payload)
    print(response.json())


if __name__ == '__main__':
    process()
